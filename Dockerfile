FROM node:8-slim as build-deps
RUN apt-get update && \ 
    apt-get install -y rsync && \
    rm -rf /var/lib/apt/lists/*
WORKDIR /got-talent/
COPY package.json yarn.lock ./
RUN yarn
COPY . .
RUN cp .env.sample .env
RUN yarn build
RUN mkdir /got-talent/prod
RUN rsync -a /got-talent/dist /got-talent/prod/ && \
    rsync -a /got-talent/node_modules /got-talent/prod/ && \
    rsync -a /got-talent/public /got-talent/prod/ && \
    rsync /got-talent/package.json /got-talent/prod/ && \
    rsync /got-talent/.env.sample /got-talent/prod/.env

FROM node:8-slim
WORKDIR /got-talent/
COPY --from=build-deps /got-talent/prod /got-talent
RUN chown -R node:node /got-talent
USER node
EXPOSE 3500
CMD ["node", "dist"]
